#!/usr/bin/env ruby
#
#NOTE about shebang line:
#   -> If you have ruby through RVM, then your shebang line should be:
#       #!/home/YOURUSERNAME/.rvm/wrappers/default/ruby
#
#   -> If you have ruby installed system-wide, then your shebang line should instead be:
#       #!/usr/bin/env ruby 
#
#NOTE made with ruby 1.9.3


#DONE:
# - test it
# - add Shw module
# - insert debug option and messages
#
#TODO:

#require 'pry'        #while writing code and cleaning bugs
require 'open-uri'

# To use this module as a CLI program, see OuiLookup::cli_run
# To use the module as an API in your own programs, see OuiLookup::lookup
#
module OuiLookup
  extend self
  VERSION = File.read(File.join(File.expand_path(File.dirname(__FILE__)),'VERSION')).lines.to_a[0].chomp     # "0.1"
  @debug_active = false

  def cli_run(args=ARGV)
    usage_txt  = <<EOT
  #{$0} v#{VERSION} https://github.com/zipizap
  
  USAGE:
    #{$0} [-d|--debug] <MAC1> [<MAC2> [... [<MACn>]]]

    (Note: multiple MACS should be space-separated)


  DESCRIPTION:
    Given an ethernet <MAC> address (00:11:22:33:44:55), it downloads and displays information about the OUI Organization ("CIMSYS Inc"),  as indicated by official IEEE listings

    This program will consult a LOCAL_OUI_FILE for info about the <MAC>
    If the LOCAL_OUI_FILE does not exist, or is older than 30 days, it will automatically download a fresh one from the IEEE site (see REMOTE_OUI_FILE_URL)"
    It will then output to stdout, the Organization corresponding to the <MAC>

        LOCAL_OUI_FILE      = "/var/tmp/oui.txt"
        REMOTE_OUI_FILE_URL = "http://standards.ieee.org/develop/regauth/oui/oui.txt"


  EXAMPLES:
    # One MAC
    $ #{$0} 00:11:22:33:44:55
      MAC                     OUI             ORGANIZATION
      00:11:22:33:44:55       00:11:22        CIMSYS Inc
 
    # Multiple MACs
    $ #{$0} 00:11:22:33:44:55 00:1a:2b:3c:4d:5e
      MAC                     OUI             ORGANIZATION
      00:11:22:33:44:55       00:11:22        CIMSYS Inc
      00:1A:2B:3C:4D:5E       00:1A:2B        Ayecom Technology Co., Ltd.
 
    
  RETURN CODES:
    0     MAC info printed correctly
    1     error
EOT
   
    #validate args
      if ARGV.size < 1 
        Shw.norm usage_txt
        exit 1
      end

      if not ARGV.select {|s| s =~ /^(-d|--debug)$/i }.empty?
        @debug_active = true 
        Shw.dbg "Found option --debug"
      end

      #macs = ARGV[-1]||""
      mac_regexp= /^(\h{2}:){5}\h{2}$/i
      macs_arr = ARGV.select {|s| s =~ mac_regexp}
      if macs_arr.empty?
        Shw.norm usage_txt
        exit 1
      else
        Shw.dbg "MACs received: macs_arr = #{macs_arr.inspect}"
      end



    #do lookup
    Shw.dbg "Going to lookup macs_arr = #{macs_arr.inspect}"
    macs_oui_info = lookup(macs_arr)
    Shw.dbg "Got macs_oui_info = #{macs_oui_info.inspect}"

    Shw.warn "MAC              \tOUI     \tORGANIZATION"
    macs_oui_info.map {|h| h.values_at(:mac,:oui,:org)*"\t"}.each {|l| Shw.norm l}

    Shw.dbg "Exiting 0"
    exit 0
  end

  # See LocalOuiFile#query_macs
  #
  def lookup(macs_arr)
    LocalOuiFile.query_macs(macs_arr)
  end
  
  
  # Its very simple to use:
  #
  #     #Query 1 mac
  #     LocalOuiFile.query_macs("00:1a:22:33:44:ff")
  #         #=> Array of Hashes
  #             mac_oui_info_arr = [ { mac:      "00:1A:22:33:44:FF",
  #                                    oui:      "00:1A:22",
  #                                    org:      "XEROX CORPORATION"      # or "" if not found
  #                                  }
  #                                ]
  #
  #     #Query an array of macs
  #     LocalOuiFile.query_macs(["00:1a:22:33:44:ff", "00:1b:22:33:44:ff"])
  #     
  # NOTE: 
  #   On each query, it will check if LOCAL_OUI_FILE_PATH exists and is fresher than 30 days. 
  #   If not, it will refresh by downloading from REMOTE_OUI_FILE_URL into LOCAL_OUI_FILE_PATH 
  #
  module LocalOuiFile
    extend self
    LOCAL_OUI_FILE_PATH = "/var/tmp/oui.txt"
    REMOTE_OUI_FILE_URL = "http://standards.ieee.org/develop/regauth/oui/oui.txt"
  
    # Will search in LOCAL_OUI_FILE_PATH the MAC information and return in a hash
    # If the LOCAL_OUI_FILE_PATH does not exist, or is older than 30 days, it will download a fresh copy from REMOTE_OUI_FILE_URL
    #
    # Arguments:
    #   macs_array Array of Strings = ["00:11:22:33:44:55", "11:22:33:44:55:66", ... ]
    #
    # Returns:
    #   mac_oui_info_arr = [ { mac:      "00:11:22:33:44:55",
    #                          oui:      "00:11:22",
    #                          org:      "XEROX CORPORATION"  or "" if not found
    #                        },
    #                        ...
    #                      ]
    #   
    def query_macs(macs_array)
      #validate args
      macs_array.each {|m| raise("bad mac '#{m}'") unless (m =~ /^(\h{2}:){5}\h{2}$/i) }
      unless fresh?                              # redownload LOCAL_OUI_FILE_PATH file unless its fresher than 30 days
        Shw.dbg "Going to refresh LOCAL_OUI_FILE_PATH with REMOTE_OUI_FILE_URL"
        refresh! 
      else
        Shw.dbg "LOCAL_OUI_FILE_PATH is fresh"
      end
      Shw.dbg "Reading LOCAL_OUI_FILE_PATH"
      local_oui_content = File.read(LOCAL_OUI_FILE_PATH) #,mode:"r:UTF-8")            # read LOCAL_OUI_FILE_PATH
      local_oui_content.encode!('UTF-8','UTF-8',:invalid => :replace)                 
            # The file oui.txt has invalid characters encoded, most probably because words from different encodings were added without properly coverting 
            # them from their original local encoding, into a normalized encoding like UTF-8.
            # So we end up with a string which is UTF-8 but contains invalid characters not recognized by UTF-8
            # When we go around executing this string in ruby code, it will start throwing errors, which we can avoid, by replacing the invalid characters
            # by valid UTF-8 characters
            # This is accomplished with the line above  clean_string = string_with_invalid_characters.encode('UTF-8','UTF-8',:invalid => :replace)
            # See 
            #   [ref1] http://yehudakatz.com/2010/05/05/ruby-1-9-encodings-a-primer-and-the-solution-for-rails/
            #   [ref2] http://stackoverflow.com/questions/2982677/ruby-1-9-invalid-byte-sequence-in-utf-8
      mac_oui_info_arr = []
      Shw.dbg "Processing macs one by one"
      macs_array.each do |mac|
        mac = mac.upcase
        mac_prefix = mac[0,8]                                         # "00:1A:22"
        mac_prefix_normalized = mac_prefix.gsub(':','-')              # "00-1A-22"
        mac_prefix_normalized_regexp = Regexp.new(Regexp.escape(mac_prefix_normalized))      
        Shw.dbg "    mac                   = #{mac.inspect}"
        Shw.dbg "    mac_prefix            = #{mac_prefix.inspect}"
        local_mac_data = local_oui_content.lines.grep(mac_prefix_normalized_regexp)[0]||""       #"00-00-FF   (hex)\t\tCAMTEC ELECTRONICS LTD.\n" or ""
        Shw.dbg "    local_mac_data        = #{local_mac_data.inspect}"

        org = local_mac_data.chomp.split("\t")[-1]||""                #"CAMTEC ELECTRONICS LTD." or ""
        Shw.dbg "    org                   = #{org.inspect}"
        mac_oui_hsh = { mac:      mac,
                        oui:      mac_prefix,
                        org:      org
                      }
        Shw.dbg "    mac_oui_hsh           = #{mac_oui_hsh.inspect}\n"
        mac_oui_info_arr << mac_oui_hsh
      end
      return mac_oui_info_arr
    end
   
    private
    # Return:
    #   True    - local oui file exists and is newer than 30 days
    #   False   - local oui file does not exist, or is older than 30 days
    def fresh?
      if File.exists?(LOCAL_OUI_FILE_PATH) && 
         File.mtime(LOCAL_OUI_FILE_PATH) >= (Time.now-3600*24*30)
        return true
      else
        return false
      end
    end
    
    # Will ovewrite the local oui file with a new downloaded copy from ieee site
    #
    def refresh!
      #File.open(LOCAL_OUI_FILE_PATH,'w:UTF-8') do |lf|
      #  open(REMOTE_OUI_FILE_URL,'r:UTF-8') do |rf|
      #    lf.write rf.read
      #  end
      #end
      File.write(LOCAL_OUI_FILE_PATH,open(REMOTE_OUI_FILE_URL).read)
    end
  end

  #
  #This will attr_reader the module-obj-instance-var @debug_active, so that outside the module, the @debug_active is readable
  class << self
    attr_reader :debug_active
  end

end #module OuiLookup

module Shw
  extend self
  def norm(msg)
    color_code = "\e[0m"
    shw(msg,color_code)
  end
  def info(msg)
    color_code = "\e[1;34m"
    shw(msg,color_code)
  end
  def warn(msg)
    color_code = "\e[1;33m"
    shw(msg,color_code)
  end
  def err(msg)
    color_code = "\e[1;31m"
    shw(("ERROR: " + msg ), color_code)
  end
  def prnt(msg)
    color_code = "\e[37m"
    shw(msg, color_code,use_print:true)
  end
  def dbg(msg)
    color_code = "\e[1;30m"
    msg = "Debug #{Time.now}\t" + msg
    shw(msg,color_code) if OuiLookup.debug_active
  end

  private
  def shw(msg,color_code,opts={})
    #init values
    output_method = :puts

    #opts overriding
    output_method = :print if opts[:use_print]

    #execute
    send(output_method, (color_code + msg + "\e[0m"))
  end
end

#if called directly, run as a cli program
OuiLookup.cli_run if File.expand_path(__FILE__) == File.expand_path($0)

