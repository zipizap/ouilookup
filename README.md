#ouilookup.rb

Given an ethernet <MAC> address (00:11:22:33:44:55), it downloads and displays information about the OUI Organization ("CIMSYS Inc"),  as indicated by official IEEE listings


#Install
    git clone git@github.com:zipizap/ouilookup.git
    #made with Ruby 1.9.3

#Usage
### As a CLI program:
`$ ./ouilookup.rb`
 
    ./ouilookup.rb v1.1 https://github.com/zipizap
  
    USAGE:
      ./ouilookup.rb [-d|--debug] <MAC1> [<MAC2> [... [<MACn>]]]
  
      (Note: multiple MACS should be space-separated)
  
  
    DESCRIPTION:
      Given an ethernet <MAC> address (00:11:22:33:44:55), it downloads and displays information about the OUI Organization ("CIMSYS Inc"),  as indicated by official IEEE listings
  
      This program will consult a LOCAL_OUI_FILE for info about the <MAC>
      If the LOCAL_OUI_FILE does not exist, or is older than 30 days, it will automatically download a fresh one from the IEEE site (see REMOTE_OUI_FILE_URL)"
      It will then output to stdout, the Organization corresponding to the <MAC>
  
          LOCAL_OUI_FILE      = "/var/tmp/oui.txt"
          REMOTE_OUI_FILE_URL = "http://standards.ieee.org/develop/regauth/oui/oui.txt"
  
  
    EXAMPLES:
      # One MAC
      $ ./ouilookup.rb 00:11:22:33:44:55
        MAC                     OUI             ORGANIZATION
        00:11:22:33:44:55       00:11:22        CIMSYS Inc
  
      # Multiple MACs
      $ ./ouilookup.rb 00:11:22:33:44:55 00:1a:2b:3c:4d:5e
        MAC                     OUI             ORGANIZATION
        00:11:22:33:44:55       00:11:22        CIMSYS Inc
        00:1A:2B:3C:4D:5E       00:1A:2B        Ayecom Technology Co., Ltd.
  
  
    RETURN CODES:
      0     MAC info printed correctly
      1     error



---

`$ ./ouilookup.rb 00:11:22:33:44:55 00:0d:dd:11:11:11`

    MAC                     OUI             ORGANIZATION
    00:11:22:33:44:55       00:11:22        CIMSYS Inc
    00:0D:DD:11:11:11       00:0D:DD        PROFÃLO TELRA ELEKTRONÃK SANAYÃ VE TÃCARET A.ÃÅ¾.



### As an API contained in a ruby module
    $ irb
    >> require './ouilookup.rb'
    true

    >> OuiLookup.lookup(["00:11:22:33:44:55"])
    [
        [0] {
            :mac => "00:11:22:33:44:55",
            :oui => "00:11:22",
            :org => "CIMSYS Inc"
        }
    ]

    >> OuiLookup.lookup(["00:11:22:33:44:55", "00:0a:22:33:44:55"])
    [
        [0] {
            :mac => "00:11:22:33:44:55",
            :oui => "00:11:22",
            :org => "CIMSYS Inc"
        },
        [1] {
            :mac => "00:0A:22:33:44:55",
            :oui => "00:0A:22",
            :org => "Amperion Inc"
        }
    ]

    >> OuiLookup.lookup(["FF:FF:FF:11:11:11"])
    [
        [0] {
            :mac => "FF:FF:FF:11:11:11",
            :oui => "FF:FF:FF",
            :org => ""
        }
    ]
    >> 



There is only one method to be used - `OuiLookup::lookup` - it takes as argument an Array of Strings (MAC addresses, like `["00:11:22:33:44:55", "aa:11:22:33:44:55"]`) and returns an Array of Hashes, containing the OUI information for each MAC.

When there is no information for a `mac` then the `org` value will be `""` , as in the last example



#Final notes
   - made for Ruby 1.9.3, no gems dependencies
   - it's not tested and may not work behind a proxy (it uses the `open-uri` library to make the download of `oui.txt` file, so if you want to make it work with a proxy, try to configure the `open-uri` lib to work with the proxy
   - it's a simple program for a simple task
   - the file oui.txt as downloaded from IEEE has invalid UTF-8 characters, which are _normalized_ inside the program in order to prevent Ruby errors

#License
Licensed under GPLv3

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/




